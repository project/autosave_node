<?php

/**
 * @file main module content for autosave_node from Techabantu.com
 */

/**
 * Implements hook_init
 */
function autosave_node_init() {
    global $user;
    $interval = variable_get('autosave_node_interval');
    $path = explode('/', current_path());
    if (count($path) >= 3) {    // add or edit
        $type = null;
        if (!($node = menu_get_object('node'))) {
            if (count($path) >= 3 && $path[0] == 'node' && $path[1] == 'add') {
                $type = $path[2];
            }
        } else if ($path[2] == 'edit') {
            $type = $node->type;
        }
        if ($type && in_array($type, array_values(variable_get('autosave_node_types', [])))) {
            drupal_add_js(drupal_get_path('module', 'autosave_node') . '/autosave.js', 'file');
            drupal_add_js(['autosave_node' => [
                'interval' => $interval, 
                'uid' => $user->uid,
                'message' => t('Problem autosaving the current document - please save your work offline and contact the administrator.
Please note that saving the document directly in the browser may not work, since autosave has found an issue with the network.
You can use the button below to dismiss this notice.')
            ]], ['type' => 'setting']);
        }
    }
}

/**
 * Implements hook_menu
 * @return array
 */
function autosave_node_menu() {
    $items = [];
    
    $items['admin/config/autosave_node'] = [
        'title'            => 'Autosave node',
        'description'      => 'Configure autosave node module',
        'page callback'    => 'drupal_get_form',
        'page arguments'   => ['autosave_node_admin_settings_form'],
        'access arguments' => ['configure autosave_node'],
    ];
    
    $items['autosave_node'] = [
        'title'            => 'Autosave',
        'description'      => 'Handle node autosave data',
        'page callback'    => 'autosave_node_rcv',
        'access callback'  => true,
    ];
    
    $items['user/%/autosaves'] = [
        'title' => 'Autosaved documents',
        'page callback' => 'drupal_get_form',
        'page arguments' => ['autosave_node_form', 1],
        'access callback' => 'user_is_logged_in',
        'type' => MENU_LOCAL_TASK,
    ];
    
    return $items;
}

/**
 * The callback for handling input data from the autosave
 */
function autosave_node_rcv() {
    $row = [
        'updated' => time(),
        'ip' => ip_address(),
    ];
    $data = [];
    // Pairs are form element name and DOM input value
    foreach (json_decode(file_get_contents('php://input')) as $pair) {
        $data[$pair->name] = $pair->value;
        if ($pair->name == 'form_build_id') {
            $row['form_build_id'] = $pair->value;
        }
        if ($pair->name == 'uid') {
            $row['uid'] = $pair->value;
        }
        if ($pair->name == 'request_path') {
            $path = explode('/', $pair->value);
            foreach ($path as $i => $s) {
                if ($s == 'node' && count($path) > $i + 1) {
                    $row['nid'] = $path[$i + 1];
                }
            }
        }
    }
    $row['data'] = serialize($data);
    drupal_write_record('autosave_node_journal', $row);
    $result = [
        'bytes' => strlen(serialize($data))
    ];
    return drupal_json_output($result);
}

/**
 * Implements hook_cron - clean up old autosaves
 */
function autosave_node_cron() {
    if (($lifetime = variable_get('autosave_node_lifetime')) > 0) {
        $lifetime *= 3600;  // Lifetime is in hours
        db_delete('autosave_node_journal')->condition('updated', time() - $lifetime, '>=')->execute();
    }
}

/**
 * Implements hook_node_presave - remove the autosaves for the node
 * @param object $node
 */
function autosave_node_node_presave($node) {
    if (in_array($node->type, array_values(variable_get('autosave_node_types', []))) && isset($node->form_build_id)) {
        db_delete('autosave_node_journal')->condition('form_build_id', $node->form_build_id)->execute();
    }
}

/**
 * The admin setting form for autosave
 * @param array $form
 * @param array $form_state
 * @return array $form
 */
function autosave_node_admin_settings_form($form, &$form_state) {
    module_load_include('inc', 'autosave_node', 'autosave_node.variable');
    $form['autosave_node_types'] = [
        '#type' => 'checkboxes',
        '#title' => t('Node types subject to autosave'),
        '#required' => false,
        '#options' => node_type_get_names(),
        '#default_value' => variable_get('autosave_node_types', [])
    ];
    $form['autosave_node_interval'] = [
        '#type' => 'textfield',
        '#title' => t('Seconds between autosaves'),
        '#required' => true,
        '#default_value' => variable_get('autosave_node_interval', 10 * AUTOSAVE_NODE_MIN_INTERVAL)
    ];
    $form['autosave_node_lifetime'] = [
        '#type' => 'textfield',
        '#title' => t('Hours to keep the journal content'),
        '#description' => t('If zero or negative, content is kept forever'),
        '#required' => false,
        '#default_value' => variable_get('autosave_node_lifetime')
    ];
    
    // TODO future: fields to hide from the user form
    
    return system_settings_form($form);
}

/**
 * The dashboard form for users to see their autosaves
 * @param array $form
 * @param array $form_state
 * @param int $uid
 * @return array $form
 */
function autosave_node_form($form, &$form_state, $uid) {
    global $user;
    if ($user->uid != 1 && $user->uid != $uid) {
        $uid = $user->uid;
    }
    
    $result = db_select('autosave_node_journal', 'aj')->fields('aj')->condition('uid', $uid)->execute()->fetchAllAssoc('asid');
    if (empty($result)) {
        return ['sorry' => ['#markup' => t('You have no autosaved documents.')]];
    }
    
    // Build field list first
    $fields = [
        'nid' => t('Node №'),
        'updated' => t('Saved'),
        'ip' => t('IP address'),
        'title' => t('Title')
    ];
    foreach ($result as $entry) {
        foreach (unserialize($entry->data) as $key => $value) {
            if (!strlen($value)) {
                continue;   // We will not create headers for empty columns
            }
            if (count($a = explode('[', $key)) == 1 || !($f = field_info_field($a[0])) || $a[count($a)-1] == 'format]' || $a[count($a)-1] == 'summary]') {
                continue;   // Not a value field
            }
            $fn = $f['field_name'];
            $name = '';
            if ($entry->nid && ($node = node_load($entry->nid)) && ($fi = field_info_instance('node', $fn, $node->type))) {
                $name = $fi['label']; // We grab the first label from any node type
            }
            if (!isset($fields[$fn]) || !strlen($fields[$fn])) {
                $fields[$fn] = $name;    // If blank will fix later
            }
        }
    }
    $row_template = [];
    foreach ($fields as $fn => $name) {
        if (!strlen($name)) {
            $fields[$fn] = t('Field @f', ['@f' => $fn]);
        }
        $row_template[$fn] = [];
    }
    
    $rows = [];
    $strings = [];
    $form_state['journal'] = $result;
    foreach ($result as $entry) {
        $row = $row_template; // Set the order of the columns
        $row['nid'] = ($entry->nid? l($entry->nid, "<a href='/node/{$entry->nid}'>{$entry->nid}</a>"): '');
        
        foreach (unserialize($entry->data) as $key => $value) {
            if (!strlen($value)) {
                continue;
            }
            if ($key == 'title') {
                $row['title'] = $value;
            } else {
                if (count($a = explode('[', $key)) == 1 || !isset($fields[$a[0]]) || $a[count($a)-1] == 'format]' || $a[count($a)-1] == 'summary]') {
                    continue;   // Not a value field
                }
                $row[$a[0]][] = $value;
            }
        }
        foreach ($row as $key => $val) {
            if (is_array($val)) {
                $row[$key] = implode('<br/>', $val);
            }
        }
        
        $string = implode('', $row); // Used for collapsing - for duplicate autosaves show only the earliest one
        if (!in_array($string, $strings)) {
            $strings[$entry->asid] = $string;
            $row['updated'] = date('Y-m-d H:i', $entry->updated);
            $row['ip'] = $entry->ip;
            $rows[$entry->asid] = $row;
        }
    }
    
    $markup = t('The "restore" button will move the selected content into the document. If the document already exists (column "node" is not empty), the original content will be overwritten.')
    . '<br/>' . t('The "delete" button will delete the selected autosave journal entry.');
    if (($lifetime = variable_get('autosave_node_lifetime')) > 0) {
        $markup .= '<br/>' . t('All autosave journal entries will be deleted automatically after @h hours.', ['@h' => $lifetime]);
    }
    $form['help'] = ['#markup' => "<p>{$markup}</p>"];

    $header = [];
    foreach ($fields as $key => $name) {
        $header[$key] = $name;
    }
    
    $options = [];
    foreach ($rows as $asid => $row) {
        $options[$asid] = [];
        foreach ($row as $key => $value) {
            $options[$asid][$key] = $value;
        }
    }
    
    $form['table'] = [
        '#type' => 'tableselect',
        '#multiple' => false,
        '#header' => $header,
        '#options' => $options,
    ];
    
    $form['restore'] = [
        '#type' => 'submit',
        '#value' => t('Restore selected content'),
        '#name' => 'restore'
        ];
    $form['delete'] = [
        '#type' => 'submit',
        '#value' => t('Delete selected autosaves'),
        '#name' => 'delete'
    ];
    
    return $form;
}

/**
 * Validate the dashboard form
 * @param array $form
 * @param array $form_state
 */
function autosave_node_form_validate(&$form, &$form_state) {
    if (empty($form_state['values']['table'])) {
        form_set_error('table', t('You must choose an item if you wish to restore or delete.'));
    }
}

/**
 * Handle submits of the dashboard form
 * @param array $form
 * @param array $form_state
 */
function autosave_node_form_submit(&$form, &$form_state) {
    $asid = $form_state['values']['table'];
    switch ($form_state['triggering_element']['#name']) {
        case 'restore':
            $row = $form_state['journal'][$asid];
            $form_id = '';
            $node_fs = ['input' => ['op' => 'Save'], 'values' => [], 'programmed' => true];
            foreach (unserialize($row->data) as $key => $value) {
                if ($key != 'uid' && $key != 'request_path') {
                    drupal_array_set_nested_value($node_fs['input'], explode('[', str_replace(']', '', $key)), $value, true);
                }
                if ($key == 'form_id') {
                    $form_id = $value;
                }
            }
            if (empty($form_id)) {
                break;
            }
            drupal_build_form($form_id, $node_fs);
            break;
            
        case 'delete':
            db_delete('autosave_node_journal')->condition('asid', $asid)->execute();
            break;

        default:
            break;
    }
}
