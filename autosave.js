/**
 * @file JavaScript for autosave node from TechAbantu.com
 */

(function ($) {
	
	var uid = 0;
	var message;

	// Attached to a form element - performs the autosave
	function autosave() {
		$('form.node-form').each(function(){
			var form = $(this).serializeArray();
			form[form.length] = {name: 'uid', value: uid};
			form[form.length] = {name: 'request_path', value: location.pathname};
			// If it is running ckeditor, we need to pull out the text by hand
			var iframes = this.getElementsByTagName("IFRAME");
			for (var i = 0; i < iframes.length; ++i) {
				var textarea = $(iframes[i]).closest('.form-textarea-wrapper').find('textarea');
				if (textarea.length > 0) {
					var id = textarea[0].name;
					var body = $(iframes[i].contentWindow.document).find('body');
					if (body.length > 0) {
						form[form.length] = {name: id, value: $(body[0]).html()};
					}
				}
			}
			
			$.ajax({
				url: '/autosave_node',
			    type: 'POST',
			    contentType: 'application/json; charset=utf-8',
			    dataType: 'json',
				data: JSON.stringify(form),
				success: function(){
				},
				error: function () {
					alert(message);
				}
			});
		});
	}
	
	Drupal.behaviors.autosave_node = {
		attach: function (context, settings) {
			const interval = Drupal.settings.autosave_node.interval;
			uid = Drupal.settings.autosave_node.uid;
			message = Drupal.settings.autosave_node.message;
			if ($('form.node-form').length > 0) {
				setInterval(autosave, interval * 1000);
			}
		}
	};

})(jQuery);
