
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Usage


INTRODUCTION
------------

Current Maintainer: Jennifer Trelewicz <http://drupal.org/user/1601538> <http://tgpo.ru>

Autosave node performs autosaves to a journaling table in case something happens to the network
connection that makes it impossible for the user to save later. We are using this on some of our
projects where the strict webserver security configurations sometimes conflict with and block
the content that users put into the node bodies, even though that content may not, in of itself,
pose a security threat. Some of our users are not technical and do not understand the best practise
of saving often. In such a case, at least what was written so far by the user is saved for possible 
restoration.

INSTALLATION
------------

Copy the 'autosave_node' module directory in to your Drupal
sites/all/modules directory and enable as you would any other module.

CONFIGURATION
-------------

Configure at admin/config/autosave_node to set the node types that are autosaved, the autosave interval,
and the lifetime of autosaves in the journaling table.

USAGE
-----

This module is intended to be used by other modules. After configuration, it will work automatically as
long as the client browser has javascript enabled. 

It is still under development, so use it at your own risk!
