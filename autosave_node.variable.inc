<?php

/**
 * @file module variables for autosave_node from Techabantu.com
 */


define('AUTOSAVE_NODE_MIN_INTERVAL', 60);   // Seconds

/**
 * Implements hook_variable_info().
 * @param $options
 * @return array of variables for autosave_node
 */
function autosave_node_variable_info($options) {
    $variables = [];
    
    $variables['autosave_node_types'] = [
        'type' => 'array',
        'title' => t('Node types for autosave node'),
        'required' => false,
        'group' => 'autosave_node',
    ];
    $variables['autosave_node_interval'] = [
        'type' => 'number',
        'title' => t('Seconds between autosaves'),
        'required' => true,
        'validate callback' => 'autosave_node_interval_validate',
        'group' => 'autosave_node',
    ];
    $variables['autosave_node_lifetime'] = [
        'type' => 'number',
        'title' => t('Hours to keep the journal'),
        'required' => true,
        'validate callback' => 'autosave_node_lifetime_validate',
        'group' => 'autosave_node',
    ];
    
    return $variables;
}

/**
 * Implements hook_variable_group_info().
 * @return array of group information for the variables
 */
function autosave_node_variable_group_info() {
    $groups = [];
    
    $groups['autosave_node'] = [
        'title' => t('autosave_node'),
        'description' => t('Configuration of autosave_node'),
        'access' => 'configure autosave_node',
        'path' => ['admin/config/autosave_node'],
    ];
    
    return $groups;
}

/**
 * Callback to validate interval
 * @param array|string $variable to be validated
 */
function autosave_node_interval_validate($variable) {
    if (is_array($variable)) {
        $variable = $variable['value'];
    }
    if (!is_numeric($variable)) {
        return t('Journal lifetime must be a number of hours.');
    }
}

/**
 * Callback to validate lifetime
 * @param array|string $variable to be validated
 */
function autosave_node_lifetime_validate($variable) {
    if (is_array($variable)) {
        $variable = $variable['value'];
    }
    $var = (int)$variable;
    if (!is_numeric($variable) || $var <= AUTOSAVE_NODE_MIN_INTERVAL || $var != $variable) {
        return t('Interval must be at least @min seconds.', ['@min' => AUTOSAVE_NODE_MIN_INTERVAL]);
    }
}
